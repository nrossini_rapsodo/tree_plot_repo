# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 12:53:04 2021

@author: nicho
"""
# plot the tree based flow chart in python

import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
from pylab import rcParams
import pandas as pd

df = pd.read_csv('analyticsTreeData.csv')

paths = df.loc[:,'data_reqs':].stack().groupby(level=0).agg(list).values.tolist()

G = nx.DiGraph()
for path in paths:
    nx.add_path(G, path)
    
rcParams['figure.figsize'] = 14, 10
pos=graphviz_layout(G, prog='dot')
nx.draw(G, pos=pos,
        node_color='lightgreen', 
        node_size=1500,
        with_labels=True, 
        arrows=True)